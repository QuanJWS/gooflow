/* 开发环境编译发布配置，非压缩编译后启动静态hot服务 */
const merge = require('webpack-merge');
const common = require('./webpack.config.js');

module.exports = merge(common,{
	devtool: 'source-map',//开发环境用
	devServer: {
		hot:true,
		contentBase: './dist'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),//启用 HMR
	]
});